@echo off
cd /d %~dp1 || pause && exit /b
for /F %%i in ('%~dp0bin\date.exe +%%Y%%m%%d%%H%%M%%S') do copy /-y %~nx1 %~nx1.%%i.backup <NUL >NUL
%~dp0bin\codec.exe -d %~nx1 %~nx1.txt >NUL
%~dp0bin\sed.exe -i s/RequiredToPlayV1//g %~nx1.txt
%~dp0bin\codec.exe -e %~nx1.txt %~nx1 >NUL
del /q %~nx1.txt
pause
